using System;
using System.Linq;
using System.Reflection;
using Boilerplate.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Boilerplate.Container {
    public class SettingsReaderInstaller : IWindsorInstaller {
        public void Install(IWindsorContainer container, IConfigurationStore store) {
            var settingsReader = new SettingsReader("../../config/settings.json");

            var settings = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(t => t.Name.EndsWith("Settings", StringComparison.InvariantCulture))
                .ToList();

            settings.ForEach(type => {
                container.Register(Component.For(type).Instance(settingsReader.LoadSection(type)));
            });
        }
    }
}
