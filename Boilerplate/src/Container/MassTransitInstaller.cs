using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Boilerplate.Configuration;
using MassTransit;
using System;

namespace Boilerplate.Container {
    public class MassTransitInstaller : IWindsorInstaller {
        public void Install(IWindsorContainer container, IConfigurationStore store) {
            var busConfig = container.Resolve<MassTransitSettings>();

            var busControl = Bus.Factory.CreateUsingRabbitMq(config => {
                config.Host(new Uri(busConfig.Host), host => {
                    host.Username(busConfig.Username);
                    host.Password(busConfig.Password);
                });

                foreach (var queue in busConfig.Queues) {
                    config.ReceiveEndpoint(queue, endpoint => {
                        endpoint.UseMessageScope();
                        endpoint.LoadFrom(container);
                    });
                }
            });

            container.Release(busConfig);
            container.Register(Component.For<IBus, IBusControl>().Instance(busControl));
        }
    }
}
