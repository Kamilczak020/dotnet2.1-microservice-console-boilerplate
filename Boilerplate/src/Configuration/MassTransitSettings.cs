namespace Boilerplate.Configuration {
    public class MassTransitSettings {
        public string Host { get; protected set; }
        public string Username { get; protected set; }
        public string Password { get; protected set; }
        public string[] Queues { get; protected set; }
    }
}
