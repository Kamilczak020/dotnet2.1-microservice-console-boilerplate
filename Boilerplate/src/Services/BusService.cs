using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.Hosting;
using MassTransit;

namespace Boilerplate.Services {
    public class BusService : IHostedService {
        private IBusControl _busControl;

        public BusService(IBusControl busControl) {
            this._busControl = busControl;
        }

        public async Task StartAsync(CancellationToken cancellationToken) {
            await this._busControl.StartAsync(cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken) {
            await this._busControl.StopAsync(cancellationToken);
        }
    }
}
