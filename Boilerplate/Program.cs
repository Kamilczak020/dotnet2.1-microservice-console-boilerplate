﻿using System;
using System.Threading.Tasks;
using Castle.Windsor;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Boilerplate.Container;
using Boilerplate.Services;

namespace Boilerplate {
    class Program {
        public static async Task Main(string[] args) {
            var builder = new HostBuilder();

            // Implement command line args
            builder.ConfigureAppConfiguration((hostingContext, config) => {
                config.AddEnvironmentVariables();

                if (args != null) {
                    config.AddCommandLine(args);
                }
            });

            // Wire up Castle Windsor as the service provider
            builder.UseServiceProviderFactory(new WindsorServiceProviderFactory());

            // Install our Windsor Container
            builder.ConfigureContainer<IWindsorContainer>((context, container) => {
                container.Install(
                    new SettingsReaderInstaller(),
                    new MassTransitInstaller()
                );
            });

            // Register all hosted services here
            builder.ConfigureServices((hostingContext, services) => {
                services.AddSingleton<BusService>();
            });

            await builder.RunConsoleAsync();
        }
    }
}
